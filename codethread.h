#ifndef CODETHREAD_H
#define CODETHREAD_H

#include <QObject>
#include <QThread>
#include <QDir>
#include <QTextStream>
#include <QDebug>

class CodeThread : public QThread
{
    Q_OBJECT
public:
    explicit CodeThread(QObject *parent = nullptr , QString customername = nullptr , QString sapcode = nullptr , QStringList list_code  = {} , int iteration = 0);

    void run() override;

private:
    QString customer_name;
    QString sapcode;
    int iteration;
    QStringList code_list;

    QFile *Qfile;
    QFile *Dbfile;
   QTextStream* quantityStream;
   QTextStream* logStream;

   int quantity;

   void generate2(QStringList list);
   void generate3(QStringList list);
   void generate4(QStringList list);
   void generate5(QStringList list);
   void generate6(QStringList list);
   void generate7(QStringList list);
   void generate8(QStringList list);
   void generate9(QStringList list);
   void generate10(QStringList list);
   void generate11(QStringList list);
   void generate12(QStringList list);
   void generate13(QStringList list);
   void generate14(QStringList list);



   void SortLargeFile(QString Dir , QString DbCode);

signals:
   void status_value(int p);
   void spliting();
   void randomizing();
   void delating();
   void finish();
   void error(QString str);

public slots:



};

#endif // CODETHREAD_H
