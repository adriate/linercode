#include "orderthread.h"

OrderThread::OrderThread(QObject *parent , QString extractCom , QString rmComm ) : QThread(parent)
{

    this->extract_command=extractCom;
    this->rmv_command=rmComm;

}

void OrderThread::run()
{
    emit extracting();
    system(extract_command.toLocal8Bit().data());
    emit deleteing();
    system(rmv_command.toLocal8Bit().data());
    emit finish();
}
