#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QDebug"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->progressBar->hide();

}

MainWindow::~MainWindow()
{
    if(CT!=nullptr)
    { CT->quit(); CT->exit(0); }
   //CT->terminate();
    delete ui;
}

void MainWindow::on_btn_newClient_clicked()
{
    NC = new NewClient_Page(this);
    connect(NC,&NewClient_Page::CodeData_Ready,this,&MainWindow::ReadyTo_Start);
    connect(NC,&NewClient_Page::PageClosed,this,&MainWindow::PageShow);
    NC->setModal(true);
    //this->hide();
    NC->show();

}

void MainWindow::on_btn_existClient_clicked()
{
    EC = new ExistClient_Page(this);
    EC->setModal(true);
    EC->show();
}

void MainWindow::ReadyTo_Start(QString customername, QString sapcode, QStringList list_code, int iteration)
{
   ui->progressBar->setRange(0,list_code.length()-1);

   CT = new CodeThread(this,customername,sapcode,list_code,iteration);
   connect(CT,&CodeThread::status_value,this,&MainWindow::status_value);
   connect(CT,&CodeThread::spliting,this,&MainWindow::spliting);
   connect(CT,&CodeThread::randomizing,this,&MainWindow::randomizing);
   connect(CT,&CodeThread::delating,this,&MainWindow::delating);
   connect(CT,&CodeThread::finish,this,&MainWindow::finish);
   connect(CT,&CodeThread::error,this,&MainWindow::error_slot);
   CT->start();
}

void MainWindow::status_value(int p)
{
    ui->progressBar->show();
    ui->progressBar->setValue(p);
    ui->progressBar->setFormat("creating db . . .");
}

void MainWindow::spliting()
{
    //ui->progressBar->setRange(0,0);
    ui->progressBar->setFormat("spliting . . .");
}

void MainWindow::randomizing()
{
   //ui->progressBar->setRange(0,0);
    ui->progressBar->setFormat("randomizing . . .");
}

void MainWindow::delating()
{
    //ui->progressBar->setRange(0,0);
    ui->progressBar->setFormat("delating . . .");
}

void MainWindow::finish()
{
    ui->progressBar->hide();
}

void MainWindow::PageShow()
{
    qInfo() << "connected";
    this->show();
}

void MainWindow::error_slot(QString str)
{
    ui->label_2->setText(str);
}
