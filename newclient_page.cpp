#include "newclient_page.h"
#include "ui_newclient_page.h"
#include <QDebug>
#include <QMessageBox>

NewClient_Page::NewClient_Page(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewClient_Page)
{
    ui->setupUi(this);
}

NewClient_Page::~NewClient_Page()
{
    delete ui;
}

void NewClient_Page::on_btnok_2_clicked()
{
   //emit PageClosed();
   this->close();
}



void NewClient_Page::on_btnok_clicked()
{
    Iteration = ui->sizebox->value();

    if(CustomerName.trimmed().isEmpty() || SapCode.trimmed().isEmpty() || Characters.trimmed().isEmpty())
    {
        QMessageBox msgBox;
        msgBox.setText("Customer name / SapCode / Code Characters cannot be empty!");
        msgBox.exec();
        return;
    }
    if(Characters.startsWith(",") || Characters.endsWith(","))
    {
        QMessageBox msgBox;
        msgBox.setText("Code Characters cannot start or ends with ´,´ !");
        msgBox.exec();
        return;
    }

    CodeData_Ready(CustomerName,SapCode,List_Chara,Iteration);

    //emit PageClosed();
    this->close();
}



// Control text in real time

void NewClient_Page::on_txtbox_customername_textChanged()
{
    if(ui->txtbox_customername->toPlainText().contains(" "))
    {
        QMessageBox msgBox;
        msgBox.setText("Customer name should not contains space!");
        msgBox.exec();
        ui->txtbox_customername->setText("");
    }
    this->CustomerName=ui->txtbox_customername->toPlainText();
}

void NewClient_Page::on_txtbox_sapcode_textChanged()
{
    if(ui->txtbox_sapcode->toPlainText().contains(" "))
    {
        QMessageBox msgBox;
        msgBox.setText("SapCode should not contains space!");
        msgBox.exec();
        ui->txtbox_sapcode->setText("");
    }
    this->SapCode=ui->txtbox_sapcode->toPlainText();
}

void NewClient_Page::on_txtbox_characters_textChanged()
{
    if(ui->txtbox_characters->toPlainText().contains(" ") ||  ui->txtbox_characters->toPlainText().contains(",,"))
    {
        QMessageBox msgBox;
        msgBox.setText("Code Characters cannot contain space or ,, !");
        msgBox.exec();
        ui->txtbox_characters->setText("");
    }
    this->Characters=ui->txtbox_characters->toPlainText();


    List_Chara = Characters.split(",", QString::SkipEmptyParts);
    if(List_Chara.removeDuplicates() >= 1)
    {
        QMessageBox msgBox;
        msgBox.setText("The same character cannot be repeated!");
        msgBox.exec();
        ui->txtbox_characters->setText("");
    }
}

