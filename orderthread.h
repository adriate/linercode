#ifndef ORDERTHREAD_H
#define ORDERTHREAD_H

#include <QObject>
#include <QThread>

class OrderThread : public QThread
{
    Q_OBJECT
public:
    explicit OrderThread(QObject *parent = nullptr , QString extractCom = "" , QString rmComm = "");
    void run();

private:

    QString extract_command;
    QString rmv_command;

signals:
    void extracting();
    void deleteing();
    void finish();

public slots:
};

#endif // ORDERTHREAD_H
