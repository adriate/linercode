#include "existclient_page.h"
#include "ui_existclient_page.h"
#include <QMimeData>
#include <QFile>


ExistClient_Page::ExistClient_Page(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ExistClient_Page)
{
    ui->setupUi(this);
    ui->treeWidget->setHeaderHidden(true);
    ui->progressBar->hide();
    ui->DONE_LBL->hide();

    //********* CUSTOMER & SAP LIST
    QDir dir(QDir::currentPath()+"/gclDb");
    QFileInfoList list = dir.entryInfoList(QDir::Dirs);
    QTreeWidgetItem* customers[list.size()];
    for (int i=0;i<list.size();i++)
    {
        if(list.at(i).fileName()!=".." && list.at(i).fileName()!="." )
        {
            // add customer to the list
            customers[i] = new QTreeWidgetItem(ui->treeWidget); customers[i]->setText(0,list.at(i).fileName());
            // find the saps
            QDir dir(QDir::currentPath()+"/gclDb"+"/"+list.at(i).fileName()); QFileInfoList sapslist = dir.entryInfoList(QDir::Dirs);
            QTreeWidgetItem* saps[sapslist.size()];
            for (int j=0;j<sapslist.size();j++)
            {
                if(sapslist.at(j).fileName()!="." && sapslist.at(j).fileName()!="..")
                {
                    saps[j] = new QTreeWidgetItem(); saps[j]->setText(0,sapslist.at(j).fileName()); customers[i]->addChild(saps[j]);
                }
            }
        }
    }
    //**********
}

void ExistClient_Page::on_treeWidget_itemClicked(QTreeWidgetItem *item, int column)
{
    if(item->parent())
    {
        sapdir = QDir::currentPath()+"/gclDb/" + item->parent()->text(column)+"/"+item->text(column);
        dbdir = QDir::currentPath()+"/gclDb/" + item->parent()->text(column)+"/"+item->text(column)+"/DB";
        ordersdir = QDir::currentPath()+"/gclDb/" + item->parent()->text(column)+"/"+item->text(column)+"/Orders";
        sapname = item->text(column);
        // Load Quantity
         Qfile = new QFile(QDir::currentPath()+"/gclDb"+"/"+item->parent()->text(column)+"/"+item->text(column)+"/Quantity");
         if(Qfile->exists()) // it cannot happens because file is created manually
         { Qfile->open(QIODevice::ReadWrite | QIODevice::Text); QTextStream in(Qfile); QString quan = in.readLine(); ui->tbquantity->setText(quan);
         quantity=quan.toInt(); Qfile->close(); }
         else
         { QMessageBox msgBox; msgBox.setText("Quantity file was not created!"); msgBox.exec();}
    }
}


void ExistClient_Page::on_btnextract_clicked()
{
    // hide last status
    ui->DONE_LBL->hide();

    int Qextra = ui->spinBoxquantity->value();
    QString Ordr = ui->tborder->toPlainText();
    if(dbdir.trimmed().isEmpty())
    {
        QMessageBox msgBox;
        msgBox.setText("Select Sapcod first!");
        msgBox.exec();
        return;
    }
    else
    {
        if(Qextra>quantity || Qextra <=0)
        {
            QMessageBox msgBox;
            msgBox.setText("Requested quantiy cannot be larger than available quantity or less than zero!");
            msgBox.exec();
            return;
        }
        if(Ordr.contains(" ") || Ordr.trimmed().isEmpty())
        {
            QMessageBox msgBox;
            msgBox.setText("Order name/number cannot contain space or be empty!");
            msgBox.exec();
            return;
        }

        QString extract_com = "sed -n 1,"+QString::number(Qextra)+"p "+ dbdir+"/DbCodeR > "+ ordersdir+"/"+sapname+"_"+Ordr;
        QString rmv_com = "sed -i 1,"+QString::number(Qextra)+"d "+ dbdir+"/DbCodeR";

        OT = new OrderThread(this,extract_com,rmv_com);
        connect(OT,&OrderThread::extracting,this,&ExistClient_Page::extracting);
        connect(OT,&OrderThread::deleteing,this,&ExistClient_Page::deleting);
        connect(OT,&OrderThread::finish,this,&ExistClient_Page::finish);
        OT->start();
    }

    quantity = quantity - Qextra;
    if(!Qfile->isOpen())
    Qfile->open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate);
    QTextStream* update = new QTextStream(Qfile);
    *update << quantity <<"\n"<<flush;
    Qfile->close();
}

void ExistClient_Page::on_btndownload_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                 "/home",
                                                 QFileDialog::ShowDirsOnly
                                                 | QFileDialog::DontResolveSymlinks);

    QFile::copy(ordersdir+"/"+sapname+"_"+ui->tborder->toPlainText(),dir+"/"+sapname+"_"+ui->tborder->toPlainText());
}

void ExistClient_Page::extracting()
{
    ui->progressBar->show();
    ui->progressBar->setValue(0);
    ui->progressBar->setStyleSheet("background-color: rgb(252, 175, 62);");
    ui->progressBar->setFormat("Extracting...");
}

void ExistClient_Page::deleting()
{
    ui->progressBar->setStyleSheet("background-color: rgb(138, 226, 52);");
    ui->progressBar->setFormat("Delating...");
}

void ExistClient_Page::finish()
{
  ui->progressBar->hide();
  ui->DONE_LBL->show();
}


void ExistClient_Page::on_pushButton_2_clicked()
{
    this->close();
}

ExistClient_Page::~ExistClient_Page()
{
    delete ui;
}

void ExistClient_Page::dropEvent(QDropEvent *ev)
{
    ev->acceptProposedAction();
    // More than one --> reject
    QList<QUrl> urls = ev -> mimeData() -> urls();
    if(urls.length() != 1)
    { ui->label_6->setText("Only One File Allowed!"); return; }
    // Is not a file --> reject
    QString path = urls[0].path();
    QFileInfo FI (path);
    if(!FI.isFile())
    { ui->label_6->setText("It needs to be file!"); return; }
    // Read to string list
    QFile* file = new QFile(path);
    file->open(QIODevice::ReadWrite | QIODevice::Text); QTextStream in(file);
    QStringList stringList;
    while (true)
    {
        QString line = in.readLine();
        if (line.isNull() )
           { break; }
        else if(line.contains(" "))
        { ui->label_6->setText("Wrong format"); return; }
        else
            stringList.append(line);
    }
    // creat and write csv file
    QFile* data = new QFile(path+".csv");
    if (!data->open(QFile::WriteOnly | QFile::Truncate |QIODevice::Append))
    {
       ui->label_6->setText("Csv file not opened"); return;
     }
    QTextStream out(data);
    int r = 0;
    while(r<stringList.length())
    {
        for (int c=0 ;c<19;c++)
        {
            if(r==stringList.length())
                break;
            if(c==0)
            out << stringList.at(r);
            else
            out <<";"<< stringList.at(r);
            r++;
        }
        out << "\n";
    }
}

void ExistClient_Page::dragEnterEvent(QDragEnterEvent *event)
{
   event->acceptProposedAction();
}

