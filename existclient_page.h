#ifndef EXISTCLIENT_PAGE_H
#define EXISTCLIENT_PAGE_H

#include <QDialog>
#include <QDir>
#include <QTreeWidget>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#include <orderthread.h>
#include <QDrag>
#include <QDropEvent>
#include <QDebug>

namespace Ui {
class ExistClient_Page;
}

class ExistClient_Page : public QDialog
{
    Q_OBJECT

public:
    explicit ExistClient_Page(QWidget *parent = nullptr);
    ~ExistClient_Page();

private slots:
    void on_pushButton_2_clicked();

    void on_treeWidget_itemClicked(QTreeWidgetItem *item, int column);

    void on_btnextract_clicked();

    void extracting();

    void deleting();

    void finish();

    void on_btndownload_clicked();

private:
    Ui::ExistClient_Page *ui;

     QFile* Qfile;
     int quantity;

     QString sapdir;
     QString dbdir;
     QString ordersdir;
     QString  sapname;

     OrderThread* OT;

protected:
    void dropEvent(QDropEvent *event) override;
    void dragEnterEvent(QDragEnterEvent *ev) override;

};

#endif // EXISTCLIENT_PAGE_H
