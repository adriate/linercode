#ifndef NEWCLIENT_PAGE_H
#define NEWCLIENT_PAGE_H

#include <QDialog>

namespace Ui {
class NewClient_Page;
}

class NewClient_Page : public QDialog
{
    Q_OBJECT

public:
    explicit NewClient_Page(QWidget *parent = nullptr);
    ~NewClient_Page();

private slots:

    void on_btnok_2_clicked();

    void on_txtbox_customername_textChanged();

    void on_txtbox_sapcode_textChanged();

    void on_txtbox_characters_textChanged();

    void on_btnok_clicked();

private:
    Ui::NewClient_Page *ui;

    QString CustomerName;
    QString SapCode;
    QString Characters;
    QStringList List_Chara;
    int Iteration;

signals:
    void CodeData_Ready(QString CustomerName , QString SapCode , QStringList List_Chara , int Iteration);
    void PageClosed();

};

#endif // NEWCLIENT_PAGE_H
