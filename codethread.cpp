#include "codethread.h"
#include <QProcess>

CodeThread::CodeThread(QObject *parent , QString customername, QString sapcode, QStringList list_code, int iteration) : QThread(parent)
{
    this->customer_name = customername;
    this->sapcode = sapcode;
    this->code_list = list_code;
    this->iteration = iteration;

    QDir().mkdir(QDir::currentPath()+"/gclDb"); //db folder
    QDir().mkdir(QDir::currentPath()+"/gclDb"+"/"+this->customer_name);
    QDir().mkdir(QDir::currentPath()+"/gclDb"+"/"+this->customer_name+"/"+this->sapcode);
    QDir().mkdir(QDir::currentPath()+"/gclDb"+"/"+this->customer_name+"/"+this->sapcode+"/DB");
    QDir().mkdir(QDir::currentPath()+"/gclDb"+"/"+this->customer_name+"/"+this->sapcode+"/Orders");

    Qfile = new QFile(QDir::currentPath()+"/gclDb"+"/"+this->customer_name+"/"+this->sapcode+"/Quantity");
    Qfile->open(QIODevice::ReadWrite | QIODevice::Text);
    quantityStream = new QTextStream(Qfile);

    Dbfile = new QFile(QDir::currentPath()+"/gclDb"+"/"+this->customer_name+"/"+this->sapcode+"/DB"+"/DbCode");
    Dbfile->open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Append);
    logStream = new QTextStream(Dbfile);
}

void CodeThread::run()
{
    if(iteration ==2)
    generate2(code_list);
    else if (iteration==3)
    generate3(code_list);
    else if (iteration==4)
    generate4(code_list);
    else if (iteration==5)
    generate5(code_list);
    else if (iteration==6)
    generate6(code_list);
    else if (iteration==7)
    generate7(code_list);
    else if (iteration==8)
    generate8(code_list);
    else if (iteration==9)
    generate9(code_list);
    else if (iteration==10)
    generate10(code_list);
//    else if (iteration==11)
//    generate11(code_list);
//    else if (iteration==12)
//    generate12(code_list);
//    else if (iteration==14)
//    generate14(code_list);



    QString DbCode = QDir::currentPath()+"/gclDb"+"/"+this->customer_name+"/"+this->sapcode+"/DB"+"/DbCode";
    QString Dbdir = QDir::currentPath()+"/gclDb"+"/"+this->customer_name+"/"+this->sapcode+"/DB";
    QString DbCodeR = QDir::currentPath()+"/gclDb"+"/"+this->customer_name+"/"+this->sapcode+"/DB"+"/DbCodeR";


    if(DbCode.size() < 1000000000) // 1Gb
    {
    emit randomizing();
    QString rand_command =  "sort -R " + DbCode + " > " + DbCodeR;
    int k = system(rand_command.toLocal8Bit().data());
    if(k!=0)
    {
        emit error("Randomization Error, process stoped");
        return;
    }
    emit finish();
    Dbfile->remove();
    }
    else
    {
    SortLargeFile(Dbdir,DbCode);
    }


}

void CodeThread::generate2(QStringList list)
{
    quantity=0;
    for ( int i=0 ;i<list.length() ; i++)
        for (int j = 0 ; j<list.length();j++)
                    {
                        *logStream<<list[i]<<list[j]<<"\n"<< flush;
                         quantity++;
                         emit status_value(i);
                    }
    *quantityStream << quantity<<"\n"<<flush;
}

void CodeThread::generate3(QStringList list)
{
    quantity=0;
    for ( int i=0 ;i<list.length() ; i++ )
        for ( int j=0 ;j<list.length() ; j++  )
            for ( int k=0 ;k<list.length() ; k++  )
                    {
                        *logStream<<list[i]<<list[j]<<list[k]<<"\n"<< flush;
                         quantity++;
                         emit status_value(i);
                     }
     *quantityStream << quantity <<"\n"<<flush;
}

void CodeThread::generate4(QStringList list)
{
    quantity=0;
    for ( int i=0 ;i<list.length() ; i++  )
        for ( int j=0 ;j<list.length() ; j++ )
            for ( int k=0 ;k<list.length() ; k++ )
                for ( int h=0 ;h<list.length() ; h++)
                    {
                        *logStream<<list[i]<<list[j]<<list[k]<<list[h]<<"\n"<< flush;
                         emit status_value(i);
                         quantity++;
                }
    *quantityStream << quantity << "\n"<<flush;
}

void CodeThread::generate5(QStringList list)
{
    quantity=0;

    for ( int i=0 ;i<list.length() ; i++  )
        for ( int j=0 ;j<list.length() ; j++  )
            for ( int k=0 ;k<list.length() ; k++  )
                for ( int h=0 ;h<list.length() ; h++  )
                    for ( int s=0 ;s<list.length() ; s++  )
                    {
                        *logStream<<list[i]<<list[j]<<list[k]<<list[h]<<list[s]<<"\n"<< flush;
                         quantity++;
                         emit status_value(i);
                    }
    *quantityStream << quantity <<"\n"<<flush;
}

void CodeThread::generate6(QStringList list)
{
    bool isnum_i,isnum_j,isnum_k,isnum_y,isnum_z,isnum_o;
    quantity=0;
    for (int i=0 ;i<list.length() ; i++  )
        for (int j=0 ;j<list.length() ; j++  )
            for (int k=0 ;k<list.length() ; k++  )
                for (int y=0 ;y<list.length() ; y++  )
                    for (int z=0 ;z<list.length() ; z++  )
                        for (int o=0 ;o<list.length() ; o++  )
                        {
                         list[i].toUInt(&isnum_i,10); list[i].toUInt(&isnum_j,10); list[i].toUInt(&isnum_k,10); list[i].toUInt(&isnum_y,10);
                         list[i].toUInt(&isnum_z,10);list[i].toUInt(&isnum_o,10);
                         if(!isnum_i && !isnum_j && !isnum_k && !isnum_y && !isnum_z && !isnum_o )
                         {
                        *logStream<<list[i]<<list[j]<<list[k]<<list[y]<<list[z]<<list[o]<<"\n"<< flush;
                         quantity++;
                         emit status_value(i);
                          }
                        }
       *quantityStream << quantity << "\n"<<flush;
}

void CodeThread::generate7(QStringList list)
{
    quantity=0;

    for (int i=0 ;i<list.length() ; i++  )
        for (int j=0 ;j<list.length() ; j++  )
            for (int k=0 ;k<list.length() ; k++  )
                for (int y=0 ;y<list.length() ; y++  )
                    for (int z=0 ;z<list.length() ; z++  )
                        for (int o=0 ;o<list.length() ; o++  )
                            for ( int p=0 ;p<list.length() ; p++  )
                    {
                        *logStream<<list[i]<<list[j]<<list[k]<<list[y]<<list[z]<<list[o]<<list[p]<<"\n"<< flush;
                         quantity++;
                         emit status_value(i);
                            }
    *quantityStream << quantity << "\n"<<flush;
}

void CodeThread::generate8(QStringList list)
{
    quantity=0;

    for (int i=0 ;i<list.length() ; i++  )
        for (int j=0 ;j<list.length() ; j++  )
            for (int k=0 ;k<list.length() ; k++  )
                for (int y=0 ;y<list.length() ; y++  )
                    for (int z=0 ;z<list.length() ; z++  )
                        for (int o=0 ;o<list.length() ; o++  )
                            for ( int p=0 ;p<list.length() ; p++  )
                                for ( int m=0 ;m<list.length() ; m++  )
                    {
                        *logStream<<list[i]<<list[j]<<list[k]<<list[y]<<list[z]<<list[o]<<list[p]<<list[m]<<"\n"<< flush;
                        quantity++;
                        emit status_value(i);
                            }
         *quantityStream << quantity << "\n"<<flush;
}

void CodeThread::generate9(QStringList list)
{
    quantity=0;

    for (int i=0 ;i<list.length() ; i++  )
        for (int j=0 ;j<list.length() ; j++  )
            for (int k=0 ;k<list.length() ; k++  )
                for (int y=0 ;y<list.length() ; y++  )
                    for (int z=0 ;z<list.length() ; z++  )
                        for (int o=0 ;o<list.length() ; o++  )
                            for ( int p=0 ;p<list.length() ; p++  )
                                for ( int m=0 ;m<list.length() ; m++  )
                                    for ( int n=0 ;n<list.length() ; n++  )
                    {
                        *logStream<<list[i]<<list[j]<<list[k]<<list[y]<<list[z]<<list[o]<<list[p]<<list[m]<<list[n]<<"\n"<< flush;
                        quantity++;
                        emit status_value(i);
                            }
          *quantityStream << quantity <<"\n"<<flush;
}

void CodeThread::generate10(QStringList list)
{
    quantity=0;

    for (int i=0 ;i<list.length() ; i++  )
        for (int j=0 ;j<list.length() ; j++  )
            for (int k=0 ;k<list.length() ; k++  )
                for (int y=0 ;y<list.length() ; y++  )
                    for (int z=0 ;z<list.length() ; z++  )
                        for (int o=0 ;o<list.length() ; o++  )
                            for ( int p=0 ;p<list.length() ; p++  )
                                for ( int m=0 ;m<list.length() ; m++  )
                                    for ( int n=0 ;n<list.length() ; n++  )
                                        for ( int u=0 ;u<list.length() ; u++  )
                    {
                        *logStream<<list[i]<<list[j]<<list[k]<<list[y]<<list[z]<<list[o]<<list[p]<<list[m]<<list[n]<<list[u]<<"\n"<< flush;
                         quantity++;
                         emit status_value(i);
                            }
            *quantityStream << quantity <<"\n"<<flush;
}


void CodeThread::SortLargeFile(QString Dir, QString DbCode)
{
    emit spliting();
    QString split_command = "split -l 500000000 "+DbCode+ " GclF";
    int k = system(split_command.toLocal8Bit().data());
    if(k!=0)
    {
        emit error("Spliting Error, process stoped");
        return;
    }
    Dbfile->remove();


    emit randomizing();
    QString MERG_command="sort -R " ;
    foreach(QFileInfo file , QDir(QDir::currentPath()).entryInfoList(QDir::NoDotAndDotDot|QDir::Files))
    {
        if(file.fileName().contains("GclF"))
        {
            MERG_command += file.fileName() +" ";
        }
    }
    MERG_command +="> DbCodeR";
    int k2 = system(MERG_command.toLocal8Bit().data());
    if(k2!=0)
    {
        emit error("Randomization error, process stoped");
        return;
    }


    emit delating();
    QString mov_command = "mv " +QDir::currentPath()+"/DbCodeR"+" " + Dir;
    int k3 = system(mov_command.toLocal8Bit().data());
    if(k3!=0)
    {
        emit error("moving files error, process stoped");
        return;
    }

    // Delating the files from the current dir
    foreach(QFileInfo file , QDir(QDir::currentPath()).entryInfoList(QDir::NoDotAndDotDot|QDir::Files))
    {
        if(file.fileName().contains("GclF"))
        {
            QString rm_command = "rm "+file.fileName();
            system(rm_command.toLocal8Bit().data());
        }
    }
    emit finish();
}


//// to be discussed ??????

//void CodeThread::generate11(QStringList list)
//{
//    quantity++;
//    for (int i=0;i<30000000;i++)
//    {
//        *logStream<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]
//                <<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<"\n"<< flush;
//        quantity++;
//    }
//      *quantityStream << quantity <<"\n"<<flush;
//}

//void CodeThread::generate12(QStringList list)
//{
//    quantity=0;
//    for (int i=0;i<30000000;i++)
//    {
//        *logStream<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]
//                <<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<"\n"<< flush;
//        quantity++;
//    }
//    *quantityStream << quantity <<"\n"<<flush;
//}

//void CodeThread::generate13(QStringList list)
//{
//    quantity=0;
//    for (int i=0;i<30000000;i++)
//    {
//        *logStream<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]
//                <<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<"\n"<< flush;
//        quantity++;
//    }
//  *quantityStream << quantity <<"\n"<<flush;
//}

//void CodeThread::generate14(QStringList list)
//{
//    quantity=0;
//    for (int i=0;i<30000000;i++)
//    {
//        *logStream<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]
//                <<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]<<list[rand()%list.length()]
//                <<list[rand()%list.length()]<<"\n"<< flush;
//        quantity++;
//    }
//    *quantityStream << quantity <<"\n"<<flush;
//}



