#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <newclient_page.h>
#include <existclient_page.h>
#include <codethread.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_btn_newClient_clicked();

    void on_btn_existClient_clicked();

    void ReadyTo_Start(QString customername , QString sapcode , QStringList list_code ,int iteration);

    void status_value(int p);
    void spliting();
    void randomizing();
    void delating();
    void finish();
    void PageShow();
    void error_slot(QString str);

private:
    Ui::MainWindow *ui;

    NewClient_Page* NC;

    ExistClient_Page* EC;

    CodeThread* CT;
};

#endif // MAINWINDOW_H
